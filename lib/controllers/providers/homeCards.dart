import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class HomeCardProvider extends ChangeNotifier{
  
  int _homeCardIndex;
  Offset _homeOffset;

  HomeCardProvider(){
    startValue();
  }


  startValue(){
    _homeCardIndex = 1;
    _homeOffset = Offset(0.0, 0.0);
  }

  List<Widget> _homeCardstList = [
    errorCard(),
    firstHomeCard(),
    secondHomeCard(),
    thirdHomeCard(),
  ];

  Widget getHomeCard() => _homeCardstList[_homeCardIndex];

  int getHomeCardIndex() => _homeCardIndex;

  Offset getHomeOffset() => _homeOffset;

  Future<void> translateHomeCard()async {
    Tween<Offset>(
    begin: const Offset(0.0, 0.0),
    end: const Offset(-200.0, 0.0));
  }

  void setHomeCard(int homeCardIndex) {
    translateHomeCard();
    if(homeCardIndex < _homeCardstList.length && homeCardIndex > 0) _homeCardIndex = homeCardIndex;
    else _homeCardIndex = 0;
    notifyListeners();
  }
}

Widget firstHomeCard() => Center(
  child: Text("First HomeCard"),
);

Widget secondHomeCard() => Center(
  child: Text("Second HomeCard"),
);

Widget thirdHomeCard() => Center(
  child: Text("Third HomeCard"),
);

Widget errorCard() => Center(
  child: Column(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      Icon(Icons.error_outline),
      Padding(
        padding: const EdgeInsets.all(20.0),
        child: Text('Erro no provider, posição não encontrada na lista'),
      ),
    ],
  ),
);