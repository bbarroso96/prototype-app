import 'package:animator/animator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:my_app/controllers/providers/homeCards.dart';
import 'package:provider/provider.dart';
import 'package:my_app/utils/colors.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return ChangeNotifierProvider<HomeCardProvider>(
        builder: (_) => HomeCardProvider(),
        child: Consumer<HomeCardProvider>(
          builder: (context, homeCardProvider, _) {
            return providerChild(context, homeCardProvider);
          },
        ),
    );
  }

  Scaffold providerChild(BuildContext context, HomeCardProvider homeCardProvider) => Scaffold(
        appBar: appBar(),
        body: body(homeCardProvider),
        bottomNavigationBar:
            Container(color: Colors.red[200], child: bottomRow(homeCardProvider)),
      );
}

Container body(HomeCardProvider homeCardProvider) => Container(
  padding: EdgeInsets.only(left: 30.0, right: 30.0, top: 50.0, bottom: 50.0),
      decoration: BoxDecoration(
        gradient: AppColors.bodyGradient,
      ),
      child: stackBody(homeCardProvider),
    );


Stack stackBody(HomeCardProvider homeCardProvider) => Stack
(
  children: <Animator>[
animatorBody(-1.1, 0.0, homeCardProvider), animatorBody(0.0, 1.1, homeCardProvider)
  ],
);

Animator animatorBody(double init, double end, HomeCardProvider homeCardProvider) => Animator(
        curve: Curves.linear,
        repeats: 0,
        tween: Tween<Offset>(
    begin: Offset(init, 0.0),
    end: Offset(end, 0.0)),
    duration: Duration(milliseconds: 400),
    cycles: 1,
    builder: (anim) => bodyChild(homeCardProvider, anim)  );

SlideTransition bodyChild(HomeCardProvider homeCardProvider, Animation anim) => SlideTransition(
  child: Container(
    height: double.maxFinite,
    width: double.maxFinite,
    decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,),
    child: homeCardProvider.getHomeCard(),
  ), position: anim,
);

AppBar appBar() => AppBar(
      elevation: 0.0,
      title: Center(
        child: Text("Thami.Md"),
      ),
      centerTitle: true,
    );

SingleChildScrollView bottomRow(HomeCardProvider homeCardProvider) => SingleChildScrollView(
    scrollDirection: Axis.horizontal,
    child: Row(
      children: <Widget>[
        rowItem((){homeCardProvider.setHomeCard(1); print('1');}, 1, homeCardProvider),
        rowItem((){homeCardProvider.setHomeCard(2);print('2');}, 2, homeCardProvider),
        rowItem((){homeCardProvider.setHomeCard(3);print('3');}, 3, homeCardProvider),
        rowItem((){homeCardProvider.setHomeCard(4);print('4');}, 4, homeCardProvider),
        rowItem((){homeCardProvider.setHomeCard(5);print('5');}, 5, homeCardProvider)
      ],
    ));

Padding rowItem(Function onTap, int index, HomeCardProvider homeCardProvider) => Padding(
    padding: EdgeInsets.all(5),
    child: InkWell(
      onTap: onTap,
      child: Container(
        alignment: Alignment.topLeft,
        width: 80,
        height: 80,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.red[300],),
        child: Stack(
          children: <Widget>[
            Icon(Icons.lightbulb_outline, color: (index == homeCardProvider.getHomeCardIndex()) ? Colors.yellow[500] : Colors.black),
            SizedBox.expand(
              child: Container(
                padding: EdgeInsets.only(left: 2.0, bottom: 5.0),
                height: double.maxFinite,
                alignment: Alignment.bottomLeft,
                child: Text("Body Content")),
            )
          ],
        ),
      ),
    ));
