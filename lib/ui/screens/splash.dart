import 'package:animator/animator.dart';
import 'package:flutter/material.dart';
import 'package:my_app/ui/animations/animations.dart';

class SplashScreen extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    Animator animations = Animations.fadeTransition(
          context:context,   
          animatedChild: Text(
            'Thami.Md',
            style: TextStyle(fontSize: 30, color: Colors.red),
          ),
          duration: 2,
          animationCurve: Curves.easeIn,
          animationCycles: 1,
          route: '/homeScreen',
          );

    return Scaffold(
        body: Center(
      child: animations,
    ));
  }
}
