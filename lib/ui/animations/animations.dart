import 'package:animator/animator.dart';
import 'package:flutter/material.dart';

class Animations {

  static Animator fadeTransition(
        {@required BuildContext context,
        @required Widget animatedChild,
        @required int duration,
        @required Curve animationCurve,
        @required int animationCycles,
        @required String route}) =>
    Animator(
      endAnimationListener: (AnimatorBloc animatorBloc) =>
          Navigator.pushReplacementNamed(context, route),
      duration: Duration(seconds: duration),
      curve: animationCurve,
      cycles: animationCycles,
      builder: (
        Animation anim,
      ) =>
          FadeTransition(
            opacity: anim,
            child: animatedChild,
          ),
    );


}