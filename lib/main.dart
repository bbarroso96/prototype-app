import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_app/ui/screens/home.dart';
import 'package:my_app/ui/screens/splash.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);

    return MaterialApp(
      title: 'Thami.Md',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      
      home: SplashScreen(),

      routes: <String, WidgetBuilder> {
      '/splashScreen': (BuildContext context) => SplashScreen(),
      '/homeScreen': (BuildContext context) => HomeScreen(),
      }
    );
  }
}
