import 'package:flutter/material.dart';

class AppColors {

  static LinearGradient bodyGradient = LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          stops: [0.1, 0.2, 0.5, 0.9],
          colors: [
            Colors.red[200],
            Colors.red[300],
            Colors.red[400],
            Colors.red[500],                
          ],
        );

  static LinearGradient bottomGradient = LinearGradient(
          begin: Alignment.bottomCenter,
          end: Alignment.topCenter,
          stops: [0.1, 0.9],
          colors: [
            Colors.red[50],
            Colors.red[200],         
          ],
        );
}